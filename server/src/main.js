const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const axios = require('axios')
const jwt = require('jsonwebtoken')
const app = express()
const port = 8080

const TOKEN_SECRET = '1994902f5dde590bdb02d119ca1b83f47e0516eeb72bb444fff9792e157377a39f553f08f6622b32807c943ad9c1f2d4588f93d99c597dd6613398c0c2f1ca76'

const authenticated = (req, res, next) => {
  const auth_header = req.headers['authorization']
  const token = auth_header && auth_header.split(' ')[1]
  if(!token)
      return res.sendStatus(401)
  jwt.verify(token, TOKEN_SECRET, (err, info) => {
      if(err) return res.sendStatus(403)
      req.username = info.username
      next()
  })
}

app.use(cors())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/api/info', authenticated, (req, res) => {
  res.send({ok: 1, username: req.username})
})

app.post('/api/login', bodyParser.json(), async (req, res) => {
    let token = req.body.token
    let result = await axios.get('https://graph.facebook.com/me', {
        params: {
            fields: 'id,name,email',
            access_token: token
        }
    })
    if(!result.data.id){
      res.sendStatus(403)
      return
    }
    let data = {
        username: result.data.email
    }
    let access_token = jwt.sign(data, TOKEN_SECRET, {expiresIn: '1800s'})
    res.send({access_token, username: data.username})

})


app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})